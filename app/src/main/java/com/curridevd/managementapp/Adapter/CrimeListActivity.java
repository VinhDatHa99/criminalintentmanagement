package com.curridevd.managementapp.Adapter;

import androidx.fragment.app.Fragment;

import com.curridevd.managementapp.UI.CrimeFragment;
import com.curridevd.managementapp.UI.SingleFragmentActivity;

public class CrimeListActivity extends SingleFragmentActivity {
	@Override
	protected Fragment createFragment() {
		return new CrimeListFragment();
	}
}
