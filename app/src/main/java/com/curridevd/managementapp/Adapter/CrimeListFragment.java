package com.curridevd.managementapp.Adapter;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.curridevd.managementapp.Model.CrimeInfo;
import com.curridevd.managementapp.Model.CrimeLab;
import com.curridevd.managementapp.R;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.zip.Inflater;

public class CrimeListFragment extends Fragment {

	private RecyclerView mCrimeRecyclerView;
	private CrimeAdapter mAdapter;
	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
							 @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_crime_list, container,false);
		mCrimeRecyclerView = view.findViewById(R.id.crime_recycler_view);
		mCrimeRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
		updateUI();
		return view;
	}
	private void updateUI()
	{
		CrimeLab crimeLab = CrimeLab.get(getActivity());
		ArrayList<CrimeInfo> crimes = crimeLab.getCrimes();
		mAdapter = new CrimeAdapter(crimes);
		mCrimeRecyclerView.setAdapter(mAdapter);
	}

	public class CrimeHolder extends RecyclerView.ViewHolder
	{
		private TextView mTitleTextView,mDateTextView;
		private CheckBox mSolvedCheckBox;
		private CrimeInfo mCrime;
		public CrimeHolder (View itemView)
		{
			super(itemView);
			mTitleTextView  = itemView.findViewById(R.id.list_item_crime_title);
			mDateTextView   = itemView.findViewById(R.id.list_item_crime_date);
			mSolvedCheckBox = itemView.findViewById(R.id.list_item_crime_checkbox_solved);
		}

		public void bindCrime(CrimeInfo crime)
		{
			mCrime = crime ;
			mTitleTextView.setText(mCrime.getTitle());
			String formatDate = DateFormat.getDateInstance(DateFormat.FULL,Locale.US).format(mCrime.getDate());
			mDateTextView.setText(formatDate);
			mSolvedCheckBox.setChecked(mCrime.isSolved());
		}
	}
	public class CrimeAdapter extends RecyclerView.Adapter<CrimeHolder>
	{
		private ArrayList<CrimeInfo> mCrimes;
		public CrimeAdapter(ArrayList<CrimeInfo> crime)
		{
			mCrimes = crime;
		}

		@Override
		public CrimeHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
			LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
			View view = layoutInflater.inflate(R.layout.list_item_crime,parent,false);
			return new CrimeHolder(view);
		}

		@Override
		public void onBindViewHolder(@NonNull CrimeHolder holder, int position) {
			CrimeInfo crime  = mCrimes.get(position);
			holder.bindCrime(crime);
		}

		@Override
		public int getItemCount() {
			return mCrimes.size();
		}
	}
}
