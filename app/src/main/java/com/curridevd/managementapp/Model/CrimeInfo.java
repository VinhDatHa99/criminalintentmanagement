package com.curridevd.managementapp.Model;

import java.util.Date;
import java.util.UUID;

public class CrimeInfo {
	private String Title;
	private UUID Id;
	private Date date;
	private boolean isSolved;

	public CrimeInfo()
	{
		Id = UUID.randomUUID();
		date = new Date();
	}

	public String getTitle() {
		return Title;
	}

	public void setTitle(String title) {
		Title = title;
	}

	public UUID getId() {
		return Id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public boolean isSolved() {
		return isSolved;
	}

	public void setSolved(boolean solved) {
		isSolved = solved;
	}
}
