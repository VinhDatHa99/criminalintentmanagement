package com.curridevd.managementapp.Model;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class CrimeLab {
//	private static CrimeLab sCrimeLab; // s prefix ==> static variable
//	private ArrayList<CrimeInfo> mCrimes;
	private static CrimeLab sCrimeLab;
	private ArrayList<CrimeInfo> mCrimes;
	private final int MAXIMUM_CRIME = 50;

	public static CrimeLab get(Context context)
	{
		if (sCrimeLab == null)
		{
			sCrimeLab = new CrimeLab(context);
		}
		return sCrimeLab;
	}
	private CrimeLab(Context context) {
		mCrimes = new ArrayList<>();
		for (int i = 0; i < MAXIMUM_CRIME; i++)
		{
			CrimeInfo crime = new CrimeInfo();
			crime.setTitle("Crime #" + i);
			crime.setSolved(i%2 == 0); //Every other one
			mCrimes.add(crime);
		}
	}
	public ArrayList<CrimeInfo> getCrimes(){
		return mCrimes;
	}

	public CrimeInfo getCrime(UUID id)
	{
		for (CrimeInfo crime : mCrimes)
		{
			if (crime.getId().equals(id))
			{
				return crime;
			}
		}
		return null;
	}
}
