package com.curridevd.managementapp.UI;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.curridevd.managementapp.Model.CrimeInfo;
import com.curridevd.managementapp.R;

import java.text.DateFormat;
import java.util.Locale;

public class CrimeFragment extends Fragment {
	private CrimeInfo crime;
	private EditText  edtTitle;
	private CheckBox  checkboxSolved;
	private Button	  btnDate;
	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		crime = new CrimeInfo();
	}

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
							 @Nullable Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.fragment_crime,container,false);

		checkboxSolved = view.findViewById(R.id.list_item_crime_checkbox_solved);
		checkboxSolved.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
				crime.setSolved(isChecked);
			}
		});

		btnDate	 = view.findViewById(R.id.crime_date);
		String dateString;
		dateString = DateFormat.getDateInstance(DateFormat.FULL, Locale.US).format(crime.getDate());
		btnDate.setText(dateString);
		btnDate.setEnabled(false); //this method use to disable the button, it will not respond even user press it

 		edtTitle = view.findViewById(R.id.crime_title);
		edtTitle.addTextChangedListener(new TextWatcher()
		{
			@Override
			public void beforeTextChanged(CharSequence charSequence, int start , int count, int after)
			{
				//Now, this field is empty
			}

			@Override
			public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
				crime.setTitle(charSequence.toString());
				//char sequence is user input
			}

			@Override
			public void afterTextChanged(Editable editable) {
				//This field is empty
			}
		});
		return view;
	}

}
